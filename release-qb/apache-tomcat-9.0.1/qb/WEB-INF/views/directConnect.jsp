<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:ipp="">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>DBSync QuickBooks App</title>

<script type="text/javascript"
	src="https://appcenter.intuit.com/Content/IA/intuit.ipp.anywhere-1.3.0.js"></script>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.3/jquery.min.js"></script>

<script>
intuit.ipp.anywhere.setup({
	menuProxy: '',
	grantUrl: '<%= request.getAttribute("dbsyncUrl")%>/qb/oauth/tp?s=QuickBooks Online&t=<%= request.getAttribute("t")%>&fn=<%= session.getAttribute("firstName")%>&ln=<%= session.getAttribute("lastName")%>&email=<%= session.getAttribute("email")%>&qba=<%= request.getAttribute("qba")%>'
});
intuit.ipp.anywhere.directConnectToIntuit();
</script>
<body>
Please wait...
</body>
</html>