<!-- This page does login to the dbsync with the given credentials
	 and redirects the user to the home page-->
<html>
<head>
<script>

function submitPage() {
	document.getElementById("Email").value = "<%= request.getAttribute("email")%>";
	document.getElementById("Passwd").value = "<%= request.getAttribute("password")%>";
	document.loginForm.submit();
}

</script>
</head>
<body onload="submitPage();">
	<form name="loginForm" action="<%= request.getAttribute("dbsyncUrl")%>/redirectToHomePage" method="post">
		<input type="hidden" value="" id="Email" name="j_username" />
		<input type="hidden" id="Passwd" name="j_password" />
		<input type="hidden" id="r" name="r" value="qbo" />
		Please wait...
	</form>
</body>
</html>